package main

import (
	"AdventOfCode/readfile"
	"fmt"
)

type node struct {
	isOnPath bool
	char     string
}

func createNodeGrid(lineArray []string) [][]node {
	grid := [][]node{}
	for _, l := range lineArray {
		row := []node{}
		for _, r := range l {
			row = append(row, node{false, string(r)})
		}
		grid = append(grid, row)
	}
	return grid
}

type location struct {
	x        int
	y        int
	distance int
	prev     *location
	next     *location
	char     string
}

func (loc *location) setProperties(x int, y int, distance int, prev location, char string) {
	loc.x = x
	loc.y = y
	loc.distance = distance
	loc.prev = &prev
	loc.char = char
}

func findStartingLocation(lineArray []string) location {
	for y, l := range lineArray {
		for x, r := range l {
			if string(r) == "S" {
				return location{x, y, 0, nil, nil, "S"}
			}
		}
	}
	return location{}
}

func findSecondLocation(lineArray []string, startLoc *location) {
	//check the 4 nodes around the starting location
	nextLocation := location{}
	if startLoc.y > 0 {
		//nodeAbove
		char := string(lineArray[startLoc.y-1][startLoc.x])
		if char == "|" || char == "7" || char == "F" {
			nextLocation.setProperties(startLoc.x, startLoc.y-1, 1, *startLoc, char)
		}
	}
	if startLoc.x < len(lineArray[startLoc.y])+1 { //node to right
		char := string(lineArray[startLoc.y][startLoc.x+1])
		if char == "-" || char == "7" || char == "J" {
			nextLocation.setProperties(startLoc.x+1, startLoc.y, 1, *startLoc, char)
		}
	}
	if startLoc.y < len(lineArray)+1 { //node below
		char := string(lineArray[startLoc.y+1][startLoc.x])
		if char == "|" || char == "L" || char == "J" {
			nextLocation.setProperties(startLoc.x, startLoc.y+1, 1, *startLoc, char)
		}
	}
	if startLoc.x > 0 { //node to left
		char := string(lineArray[startLoc.y][startLoc.x-1])
		if char == "-" || char == "L" || char == "F" {
			nextLocation.setProperties(startLoc.x-1, startLoc.y, 1, *startLoc, char)
		}
	}
	startLoc.next = &nextLocation
}

func followPath(lineArray []string, currLocation *location, startingLoc *location) {
	nextLocation := location{}
	switch currLocation.char {
	case "|": //prev is either above or below
		if currLocation.prev.y > currLocation.y {
			nextLocation.setProperties(currLocation.x, currLocation.y-1, currLocation.distance+1, *currLocation, string(lineArray[currLocation.y-1][currLocation.x])) //below, so next is above
		} else {
			nextLocation.setProperties(currLocation.x, currLocation.y+1, currLocation.distance+1, *currLocation, string(lineArray[currLocation.y+1][currLocation.x])) //above, so next is below
		}
	case "-": //prev is either left or right
		if currLocation.prev.x > currLocation.x {
			nextLocation.setProperties(currLocation.x-1, currLocation.y, currLocation.distance+1, *currLocation, string(lineArray[currLocation.y][currLocation.x-1])) //right, so next is left
		} else {
			nextLocation.setProperties(currLocation.x+1, currLocation.y, currLocation.distance+1, *currLocation, string(lineArray[currLocation.y][currLocation.x+1])) //left, so next is right
		}
	case "L": //prev is either above or right
		if currLocation.prev.y != currLocation.y {
			nextLocation.setProperties(currLocation.x+1, currLocation.y, currLocation.distance+1, *currLocation, string(lineArray[currLocation.y][currLocation.x+1])) //above, so next is right
		} else {
			nextLocation.setProperties(currLocation.x, currLocation.y-1, currLocation.distance+1, *currLocation, string(lineArray[currLocation.y-1][currLocation.x])) //right, so next is above
		}
	case "J": //prev is either above or left
		if currLocation.prev.y != currLocation.y {
			nextLocation.setProperties(currLocation.x-1, currLocation.y, currLocation.distance+1, *currLocation, string(lineArray[currLocation.y][currLocation.x-1])) //above, so next is left
		} else {
			nextLocation.setProperties(currLocation.x, currLocation.y-1, currLocation.distance+1, *currLocation, string(lineArray[currLocation.y-1][currLocation.x])) //left, so next is above
		}
	case "F": //prev is either below or right
		if currLocation.prev.y != currLocation.y {
			nextLocation.setProperties(currLocation.x+1, currLocation.y, currLocation.distance+1, *currLocation, string(lineArray[currLocation.y][currLocation.x+1])) //below, so next is right
		} else {
			nextLocation.setProperties(currLocation.x, currLocation.y+1, currLocation.distance+1, *currLocation, string(lineArray[currLocation.y+1][currLocation.x])) //right, so next is below
		}
	case "7": //prev is either below or left
		if currLocation.prev.y != currLocation.y {
			nextLocation.setProperties(currLocation.x-1, currLocation.y, currLocation.distance+1, *currLocation, string(lineArray[currLocation.y][currLocation.x-1])) //below, so next is left
		} else {
			nextLocation.setProperties(currLocation.x, currLocation.y+1, currLocation.distance+1, *currLocation, string(lineArray[currLocation.y+1][currLocation.x])) //left, so next is below
		}
	}

	if nextLocation.char == "S" {
		startingLoc.prev = currLocation
		currLocation.next = startingLoc
	} else {
		currLocation.next = &nextLocation
		followPath(lineArray, &nextLocation, startingLoc)
	}
}

func part1(lineArray []string) {
	startingLocation := findStartingLocation(lineArray)
	findSecondLocation(lineArray, &startingLocation)
	followPath(lineArray, startingLocation.next, &startingLocation)
	curNode := &startingLocation
	distance := 0
	highestDistance := 0
	for curNode.prev.char != "S" {
		if distance < curNode.distance {
			curNode.distance = distance
		}
		distance += 1
		if curNode.distance > highestDistance {
			highestDistance = curNode.distance
		}
		curNode = curNode.prev
	}
	fmt.Printf("The farthest location is %d steps away\n", highestDistance)
}

func part2(lineArray []string) {
	startingLocation := findStartingLocation(lineArray)
	findSecondLocation(lineArray, &startingLocation)
	followPath(lineArray, startingLocation.next, &startingLocation)
	grid := createNodeGrid(lineArray)

	curNode := &startingLocation
	for curNode.next.char != "S" {
		grid[curNode.y][curNode.x].isOnPath = true
		curNode = curNode.next
	}
	grid[startingLocation.prev.y][startingLocation.prev.x].isOnPath = true

	countNodes := 0
	for y, r := range grid {
		insideGrid := false
		openChar := "."
		for x, n := range r {
			if n.isOnPath && n.char != "-" {
				if n.char == "7" {
					if openChar == "L" {
						openChar = n.char
						continue
					}
				} else if n.char == "J" {
					if openChar == "F" {
						openChar = n.char
						continue
					}
				}

				openChar = n.char
				insideGrid = !insideGrid
			}
			if insideGrid && !n.isOnPath {
				fmt.Printf("Counting %s node at %d, %d\n", n.char, x, y)
				countNodes += 1
			}
		}
	}

	fmt.Printf("The total number of nodes inside the loop is %d \n", countNodes)
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	part1(lineArray)
	part2(lineArray)
}
