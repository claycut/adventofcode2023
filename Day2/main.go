package main

import (
	"AdventOfCode/readfile"
	"fmt"
	"strconv"
	"strings"
)

type game struct {
	id    int
	hands [][]gameHand
}

type gameHand struct {
	count int
	color string
}

func readGameData(l string) game {
	//strip the Game ID
	gameIdSplit := strings.Split(l, ": ")
	id, _ := strconv.Atoi(gameIdSplit[0][5:])
	hands := [][]gameHand{}
	//split on ;
	handSplit := strings.Split(gameIdSplit[1], "; ")
	for _, hStr := range handSplit {
		oneHand := []gameHand{}
		//split on ,
		diceSplit := strings.Split(hStr, ", ")
		for _, d := range diceSplit {
			numDice := strings.Split(d, " ")
			count, _ := strconv.Atoi(numDice[0])
			oneHand = append(oneHand, gameHand{count, numDice[1]})
		}
		hands = append(hands, oneHand)
	}
	return game{id, hands}
}

func part1(lineArray []string) {
	// 12 red cubes, 13 green cubes, and 14 blue cubes
	sum := 0
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		game := readGameData(l)
		isPossible := true
		for _, gh := range game.hands {
			for _, oh := range gh {
				if (oh.color == "red" && oh.count > 12) || (oh.color == "green" && oh.count > 13) || (oh.color == "blue" && oh.count > 14) {
					//this is impossible game
					isPossible = false
				}
			}
		}
		if isPossible {
			sum += game.id
		}
	}
	fmt.Printf("The sum of possible games = %d\n", sum)
}

func part2(lineArray []string) {
	sum := 0
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		game := readGameData(l)
		minRed := 0
		minBlue := 0
		minGreen := 0
		for _, gh := range game.hands {
			for _, oh := range gh {
				if oh.color == "red" && oh.count > minRed {
					minRed = oh.count
				}
				if oh.color == "blue" && oh.count > minBlue {
					minBlue = oh.count
				}
				if oh.color == "green" && oh.count > minGreen {
					minGreen = oh.count
				}
			}
		}
		sum += (minRed * minBlue * minGreen)
	}
	fmt.Printf("The sum of the power is %d\n", sum)
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	part1(lineArray)
	part2(lineArray)
}
