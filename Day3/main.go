package main

import (
	"AdventOfCode/readfile"
	"fmt"
	"strconv"
	"unicode"
)

// return true if it is a valid part number
func checkIfPartNumber(l int, firstIndex int, lastIndex int, lineArray []string) bool {
	//check lines above
	fi := firstIndex
	if firstIndex > 0 {
		fi = firstIndex - 1
		//check side
		if string(lineArray[l][fi]) != "." && unicode.IsDigit(rune(lineArray[l][fi])) == false {
			return true
		}
	}
	li := lastIndex
	if lastIndex < len(lineArray[l])-1 {
		li = lastIndex + 1
		//check sides
		if string(lineArray[l][li]) != "." && unicode.IsDigit(rune(lineArray[l][li])) == false {
			return true
		}
	}

	if l > 0 {
		for _, c := range lineArray[l-1][fi : li+1] {
			if string(c) != "." && unicode.IsDigit(c) == false {
				return true
			}
		}
	}

	//check lines below
	if l < len(lineArray)-1 {
		for _, c := range lineArray[l+1][fi : li+1] {
			if string(c) != "." && unicode.IsDigit(c) == false {
				return true
			}
		}
	}
	return false
}

// return part number
func getPartNumber(line string, index int) int {
	startIndex := index
	endIndex := index
	//check indcies to left
	for i := index; i >= 0; i-- {
		if unicode.IsDigit(rune(line[i])) {
			startIndex = i
		} else {
			break
		}
	}

	//check indices to right
	for i := index; i < len(line); i++ {
		if unicode.IsDigit(rune(line[i])) {
			endIndex = i
		} else {
			break
		}
	}

	value, _ := strconv.Atoi(line[startIndex : endIndex+1])
	return value
}

// return gear ratio if a gear
func checkIfGear(l int, index int, lineArray []string) int {
	val1 := 0
	val2 := 0

	fi := index
	if index > 0 {
		fi = index - 1
		//check side
		if unicode.IsDigit(rune(lineArray[l][fi])) {
			v := getPartNumber(lineArray[l], fi)
			if val1 == 0 {
				val1 = v
			} else {
				val2 = v
			}
		}
	}
	li := index
	if index < len(lineArray[l])-1 {
		li = index + 1
		//check sides
		if unicode.IsDigit(rune(lineArray[l][li])) {
			v := getPartNumber(lineArray[l], li)
			if val1 == 0 {
				val1 = v
			} else {
				val2 = v
			}
		}
	}

	//check lines above
	if l > 0 {
		foundDigit := false
		for n, c := range lineArray[l-1][fi : li+1] {
			if unicode.IsDigit(c) && !foundDigit {
				foundDigit = true
				v := getPartNumber(lineArray[l-1], n+fi)
				if val1 == 0 {
					val1 = v
				} else {
					val2 = v
				}
			}
			if foundDigit == true && unicode.IsDigit(c) == false {
				foundDigit = false
			}

		}
	}

	//check lines below
	if l < len(lineArray)-1 {
		foundDigit := false
		for n, c := range lineArray[l+1][fi : li+1] {
			if unicode.IsDigit(c) && !foundDigit {
				foundDigit = true
				v := getPartNumber(lineArray[l+1], n+fi)
				if val1 == 0 {
					val1 = v
				} else {
					val2 = v
				}
			}
			if foundDigit == true && unicode.IsDigit(c) == false {
				foundDigit = false
			}
		}
	}
	return val1 * val2
}

func part1(lineArray []string) {
	sum := 0
	for i, l := range lineArray {
		isLastCharDigit := false
		firstIndex := -1
		lastIndex := -1
		for n, r := range l {
			if unicode.IsDigit(r) {
				isLastCharDigit = true
				if firstIndex == -1 {
					firstIndex = n
				}
				lastIndex = n + 1
			} else {
				if isLastCharDigit {
					if checkIfPartNumber(i, firstIndex, lastIndex-1, lineArray) {
						value, _ := strconv.Atoi(l[firstIndex:lastIndex])
						sum += value
					}
				}
				isLastCharDigit = false
				firstIndex = -1
				lastIndex = -1
			}
		}
		if isLastCharDigit {
			if checkIfPartNumber(i, firstIndex, lastIndex-1, lineArray) {
				value, _ := strconv.Atoi(l[firstIndex:lastIndex])
				sum += value
			}
		}
	}
	fmt.Printf("Partnumber sum = %d\n", sum)
}

func part2(lineArray []string) {
	sum := 0
	for i, l := range lineArray {
		for n, r := range l {
			if string(r) == "*" {
				sum += checkIfGear(i, n, lineArray)
			}
		}
	}
	fmt.Printf("Gear sum = %d\n", sum)
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	part1(lineArray)
	part2(lineArray)
}
