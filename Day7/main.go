package main

import (
	"AdventOfCode/readfile"
	"fmt"
	"sort"
	"strconv"
	"strings"
)

type value int

const (
	joker value = iota
	two
	three
	four
	five
	six
	seven
	eight
	nine
	T
	J
	Q
	K
	A
)

type handType int

const (
	high handType = iota
	pair
	pair2
	kind3
	full
	kind4
	kind5
)

type hand struct {
	cards []value
	bid   int
	hType handType
}

func (h *hand) calcType() {
	cardMap := make(map[value]int)
	jokerCount := 0
	keys := []value{}
	for _, c := range h.cards {
		if c == joker {
			jokerCount += 1
		} else {
			cardMap[c] += 1
			keys = append(keys, c)
		}
	}
	if jokerCount > 0 {
		highestCount := 0
		key := joker //can't really be joker here
		for k, v := range cardMap {
			if v > highestCount {
				key = k
				highestCount = v
			}
		}
		cardMap[key] += jokerCount
	}

	switch len(cardMap) {
	case 1:
		h.hType = kind5
	case 2:
		count := cardMap[keys[0]]
		if count == 1 || count == 4 {
			h.hType = kind4
		} else {
			h.hType = full
		}
	case 3:
		if cardMap[keys[0]] == 2 || cardMap[keys[1]] == 2 {
			h.hType = pair2
		} else {
			h.hType = kind3
		}
	case 4:
		h.hType = pair
	case 5:
		h.hType = high
	}
}

type ByHandType []hand

func (a ByHandType) Len() int      { return len(a) }
func (a ByHandType) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a ByHandType) Less(i, j int) bool {
	if a[i].hType == a[j].hType {
		for n := 0; n < len(a[i].cards); n++ {
			if a[i].cards[n] == a[j].cards[n] {
				continue
			}
			return a[i].cards[n] < a[j].cards[n]
		}
	}
	return a[i].hType < a[j].hType
}

func getValue(r rune) value {
	switch r {
	case '1':
		return joker
	case '2':
		return two
	case '3':
		return three
	case '4':
		return four
	case '5':
		return five
	case '6':
		return six
	case '7':
		return seven
	case '8':
		return eight
	case '9':
		return nine
	case 'T':
		return T
	case 'J':
		return J
	case 'Q':
		return Q
	case 'K':
		return K
	default:
		return A
	}
}

func parseHands(lineArray []string, hasJoker bool) []hand {
	allHands := []hand{}
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		hSplit := strings.Split(l, " ")
		cards := []value{}
		for _, r := range hSplit[0] {
			if r == 'J' && hasJoker {
				r = '1'
			}
			cards = append(cards, getValue(r))
		}
		bid, _ := strconv.Atoi(hSplit[1])
		h := hand{cards, bid, 0}
		h.calcType()
		allHands = append(allHands, h)
	}
	return allHands
}

func part1(lineArray []string) {
	allHands := parseHands(lineArray, false)
	sort.Sort(ByHandType(allHands))

	totalWinnings := 0
	for i := 0; i < len(allHands); i++ {
		score := allHands[i].bid * (i + 1)
		totalWinnings += score
	}

	fmt.Printf("The total winnings are %d\n", totalWinnings)
}

func part2(lineArray []string) {
	allHands := parseHands(lineArray, true)
	sort.Sort(ByHandType(allHands))

	totalWinnings := 0
	for i := 0; i < len(allHands); i++ {
		score := allHands[i].bid * (i + 1)
		totalWinnings += score
	}
	fmt.Printf("The total winnings are %d\n", totalWinnings)
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	part1(lineArray)
	part2(lineArray)
}
