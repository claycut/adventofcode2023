package main

import (
	"AdventOfCode/readfile"
	"fmt"
	"strconv"
	"strings"
)

type card struct {
	id             string
	numbers        []int
	winningNumbers []int
	instances      int
}

func (c card) scoreCard() int {
	score := 0
	for _, wn := range c.winningNumbers {
		for _, n := range c.numbers {
			if n == wn {
				if score == 0 {
					score = 1
				} else {
					score = score + score
				}
			}
		}
	}
	return score
}

func (c card) getNumCards() int {
	numCards := 0
	for _, wn := range c.winningNumbers {
		for _, n := range c.numbers {
			if n == wn {
				numCards += 1
			}
		}
	}
	return numCards
}

func loadCards(lineArray []string) []card {
	cardList := []card{}
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		idSplit := strings.Split(l, ":")
		id := strings.TrimSpace(strings.ReplaceAll(idSplit[0], "Card", ""))
		pipeSplit := strings.Split(idSplit[1], "|")
		numSplit := strings.Split(strings.TrimSpace(pipeSplit[0]), " ")
		winningNumbers := []int{}
		for _, n := range numSplit {
			if n == "" {
				continue
			}
			num, _ := strconv.Atoi(strings.TrimSpace(n))
			winningNumbers = append(winningNumbers, num)
		}
		numSplit = strings.Split(strings.TrimSpace(pipeSplit[1]), " ")
		numbers := []int{}
		for _, n := range numSplit {
			if n == "" {
				continue
			}
			num, _ := strconv.Atoi(strings.TrimSpace(n))
			numbers = append(numbers, num)
		}
		cardList = append(cardList, card{id, numbers, winningNumbers, 1})
	}
	return cardList
}

func part1(lineArray []string) {
	cardList := loadCards(lineArray)
	scoreSum := 0
	for _, c := range cardList {
		scoreSum += c.scoreCard()
	}
	fmt.Printf("The score of all cards = %d\n", scoreSum)
}

func part2(lineArray []string) {
	cardList := loadCards(lineArray)
	cardCount := 0
	for i, c := range cardList {
		nc := c.getNumCards()
		for n := i + 1; n <= i+nc; n++ {
			cardList[n].instances += cardList[i].instances
		}
	}
	for _, c := range cardList {
		cardCount += c.instances
	}
	fmt.Printf("The total number of cards = %d\n", cardCount)
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	part1(lineArray)
	part2(lineArray)
}
