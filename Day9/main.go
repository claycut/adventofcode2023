package main

import (
	"AdventOfCode/readfile"
	"fmt"
	"strconv"
	"strings"
)

func predictFuture(history [][]int) int {
	value := 0
	for i := len(history) - 2; i >= 0; i-- {
		value += history[i][len(history[i])-1]
	}
	return value
}

func computePast(history [][]int) int {
	value := 0
	for i := len(history) - 1; i > 0; i-- {
		value = history[i-1][0] - value
	}
	return value
}

func populateArrays(input []int) [][]int {
	output := [][]int{}
	output = append(output, input)
	i := 0
	for {
		result, isLast := getDifferenceArray(output[i])
		output = append(output, result)
		if isLast {
			break
		}
		i++
	}
	return output
}

func getDifferenceArray(input []int) ([]int, bool) {
	output := []int{}
	isLast := true
	for i, _ := range input {
		if i != len(input)-1 {
			diff := input[i+1] - input[i]
			output = append(output, diff)
			if diff != 0 {
				isLast = false
			}
		}
	}
	return output, isLast
}

func part1(lineArray []string) {
	sum := 0
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		lineSplit := strings.Split(l, " ")
		array := []int{}
		for _, c := range lineSplit {
			v, _ := strconv.Atoi(c)
			array = append(array, v)
		}
		v := predictFuture(populateArrays(array))
		sum += v
		fmt.Printf("The next value will be %d \n", v)
	}

	fmt.Printf("The sum of all predicted values is %d \n", sum)
}

func part2(lineArray []string) {
	sum := 0
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		lineSplit := strings.Split(l, " ")
		array := []int{}
		for _, c := range lineSplit {
			v, _ := strconv.Atoi(c)
			array = append(array, v)
		}
		v := computePast(populateArrays(array))
		sum += v
		fmt.Printf("The next value will be %d \n", v)
	}

	fmt.Printf("The sum of all extrapolated values is %d \n", sum)
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	part1(lineArray)
	part2(lineArray)
}
