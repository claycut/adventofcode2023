package main

import (
	"fmt"
	"strconv"
	"unicode"

	"AdventOfCode/readfile"
)

func part1(lineArray []string) {
	sum := 0
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		firstDigit := ""
		lastDigit := ""
		for _, c := range l {
			if unicode.IsDigit(c) {
				if firstDigit == "" {
					firstDigit = string(c)
				}
				lastDigit = string(c)
			}
		}
		value, _ := strconv.Atoi(firstDigit + lastDigit)
		sum += value
	}
	fmt.Printf("The sum of all lines = %d\n", sum)
}

func convertDigit(index int, line string) string {
	numStrArray := []string{"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"}
	for i, numStr := range numStrArray {
		if len(line[index:]) >= len(numStr) && numStr[0] == line[index] {
			if numStr == line[index:index+len(numStr)] {
				return strconv.Itoa(i + 1)
			}
		}
	}
	return ""
}

func part2(lineArray []string) {
	sum := 0
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		firstDigit := ""
		lastDigit := ""
		for i, c := range l {
			digit := convertDigit(i, l)
			if unicode.IsDigit(c) {
				if firstDigit == "" {
					firstDigit = string(c)
				}
				lastDigit = string(c)
			} else if digit != "" {
				if firstDigit == "" {
					firstDigit = digit
				}
				lastDigit = digit
			}
		}
		value, _ := strconv.Atoi(firstDigit + lastDigit)
		sum += value
	}
	fmt.Printf("The sum of all lines = %d\n", sum)
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	part1(lineArray)
	part2(lineArray)
}
