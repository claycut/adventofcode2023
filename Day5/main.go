package main

import (
	"AdventOfCode/readfile"
	"fmt"
	"math"
	"strconv"
	"strings"
)

type almanacMap struct {
	source      string
	destination string
	sourceMap   *[]mapEntries
}

type mapEntries struct {
	startSource int
	startDest   int
	mapRange    int
}

type seedRange struct {
	start int
	r     int
}

func (aMap almanacMap) getValue(key int) int {
	for _, e := range *aMap.sourceMap {
		if e.startSource <= key && e.startSource+e.mapRange-1 >= key {
			return e.startDest + (key - e.startSource)
		}
	}
	return key
}

func (aMap almanacMap) getValueRange(sr seedRange) []seedRange {
	currSR := sr
	value := []seedRange{}
	for _, e := range *aMap.sourceMap {
		if sr.start < e.startSource && sr.start+sr.r > e.startSource { //if the starting value <= the map source then the startingValue + range must be > map source
			// remove values off the front
			newStart := currSR.start
			newRange := e.startSource - newStart
			value = append(value, aMap.getValueRange(seedRange{newStart, newRange})...)
			currSR.start = currSR.start + newRange
			currSR.r = currSR.r - newRange
			if currSR.start+currSR.r > e.startSource+e.mapRange { // have to store the end range too (could treat this recursively)
				// remove values off the back
				newStart := e.startSource + e.mapRange
				newRange := (currSR.start + currSR.r) - (e.startSource + e.mapRange)
				value = append(value, aMap.getValueRange(seedRange{newStart, newRange})...)
				currSR.r = currSR.r - newRange
			}
			return append(value, seedRange{e.startDest, currSR.r}) // append the value from the destination
		} else if currSR.start >= e.startSource && currSR.start < e.startSource+e.mapRange { //there is some overlap, but the first index is not inside
			//the first index starts inside
			if currSR.start+currSR.r > e.startSource+e.mapRange { // have to store the end range too (could treat this recursively)
				// remove values off the back
				newStart := e.startSource + e.mapRange
				newRange := (currSR.start + currSR.r) - (e.startSource + e.mapRange)
				value = append(value, aMap.getValueRange(seedRange{newStart, newRange})...)
				currSR.r = currSR.r - newRange
			}
			difference := currSR.start - e.startSource
			newStart := e.startDest + difference
			newRange := currSR.r
			return append(value, seedRange{newStart, newRange}) // append the value from the destination
		}
	}
	// if anything left if currSR, return back same values
	return append(value, currSR)

}

func getLowestLocationValueForRange(aMap map[string]almanacMap, src string, srArray []seedRange) []seedRange {
	if src == "location" {
		return srArray
	}
	newSeedArray := []seedRange{}
	for _, sr := range srArray {
		newSeedArray = append(newSeedArray, aMap[src].getValueRange(sr)...)
	}
	return getLowestLocationValueForRange(aMap, aMap[src].destination, newSeedArray)
}

func getLocationValue(aMap map[string]almanacMap, src string, key int) int {
	if src == "location" {
		return key
	}
	return getLocationValue(aMap, aMap[src].destination, aMap[src].getValue(key))
}

func createAlmanac(lineArray []string) ([]int, map[string]almanacMap) {
	seeds := []int{}
	almanac := make(map[string]almanacMap)
	currSource := ""
	for _, l := range lineArray {
		if strings.HasPrefix(l, "seeds:") {
			seedSplit := strings.Split(l, " ")
			for _, s := range seedSplit {
				if s != "seeds:" {
					seedNum, _ := strconv.Atoi(s)
					seeds = append(seeds, seedNum)
				}
			}
		} else if strings.Contains(l, " map") {
			name := strings.ReplaceAll(l, " map:", "")
			nameSplit := strings.Split(name, "-")
			almanac[nameSplit[0]] = almanacMap{nameSplit[0], nameSplit[2], &[]mapEntries{}}
			currSource = nameSplit[0]
		} else if l != "" {
			numSplit := strings.Split(l, " ")
			srcNum, _ := strconv.Atoi(numSplit[1])
			destNum, _ := strconv.Atoi(numSplit[0])
			rangeNum, _ := strconv.Atoi(numSplit[2])
			*almanac[currSource].sourceMap = append(*almanac[currSource].sourceMap, mapEntries{srcNum, destNum, rangeNum})
		}
	}
	return seeds, almanac
}

func part1(lineArray []string) {
	seeds, almanac := createAlmanac(lineArray)
	lowestLocation := math.MaxInt
	for _, seed := range seeds {
		locationValue := getLocationValue(almanac, "seed", seed)
		fmt.Printf("The final location for seed %d = %d\n", seed, locationValue)
		if locationValue < lowestLocation {
			lowestLocation = locationValue
		}
	}
	fmt.Printf("The lowest location value = %d\n", lowestLocation)

}

func part2(lineArray []string) {
	seeds, almanac := createAlmanac(lineArray)
	lowestLocation := math.MaxInt

	newSeeds := []seedRange{}
	for i := 0; i < len(seeds); i++ {
		if i%2 == 0 {
			newSeeds = append(newSeeds, seedRange{seeds[i], seeds[i+1]})
			fmt.Printf("Created SR = %d:%d\n", seeds[i], seeds[i+1])
		}
	}

	locationValue := getLowestLocationValueForRange(almanac, "seed", newSeeds)
	for _, sr := range locationValue {
		if sr.start < lowestLocation {
			lowestLocation = sr.start
		}
	}
	fmt.Printf("The lowest location value = %d\n", lowestLocation)
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	part1(lineArray)
	part2(lineArray)
}
