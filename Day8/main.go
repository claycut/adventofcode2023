package main

import (
	"AdventOfCode/readfile"
	"fmt"
	"strings"
)

type move struct {
	id    string
	left  string
	right string
}

func part1(lineArray []string) {
	moveMap := make(map[string]move)
	dirs := ""
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		if strings.Contains(l, "=") {
			l = strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(l, " ", ""), "(", ""), ")", "")
			lSplit := strings.Split(l, "=")
			id := lSplit[0]
			lrSplit := strings.Split(lSplit[1], ",")
			m := move{id, lrSplit[0], lrSplit[1]}
			moveMap[id] = m
		} else {
			dirs = l
		}
	}

	stepsCount := 0
	location := "AAA"
	for location != "ZZZ" {
		for _, d := range dirs {
			if string(d) == "L" {
				location = moveMap[location].left
			} else {
				location = moveMap[location].right
			}
			stepsCount += 1
			if location == "ZZZ" {
				break
			}
		}
	}

	fmt.Printf("The total steps from AAA to ZZZ is %d \n", stepsCount)
}

func isComplete(loc string) bool {
	return strings.HasSuffix(loc, "Z")
}

// greatest common divisor (GCD) via Euclidean algorithm (https://siongui.github.io/2017/06/03/go-find-lcm-by-gcd/)
func GCD(a, b int) int {
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	return a
}

// find Least Common Multiple (LCM) via GCD (https://siongui.github.io/2017/06/03/go-find-lcm-by-gcd/)
func LCM(a, b int, integers ...int) int {
	result := a * b / GCD(a, b)

	for i := 0; i < len(integers); i++ {
		result = LCM(result, integers[i])
	}
	return result
}

func part2(lineArray []string) {
	moveMap := make(map[string]move)
	dirs := ""
	startingKeys := []string{}
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		if strings.Contains(l, "=") {
			l = strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(l, " ", ""), "(", ""), ")", "")
			lSplit := strings.Split(l, "=")
			id := lSplit[0]
			lrSplit := strings.Split(lSplit[1], ",")
			m := move{id, lrSplit[0], lrSplit[1]}
			moveMap[id] = m
			if strings.HasSuffix(id, "A") {
				startingKeys = append(startingKeys, id)
			}
		} else {
			dirs = l
		}
	}

	locationArray := startingKeys
	// const len = len(locationArray)
	stepCountArray := []int{}
	for i, loc := range locationArray {
		stepCountArray = append(stepCountArray, 0)
		for !isComplete(loc) {
			for _, d := range dirs {
				// for i, loc := range locationArray {
				if string(d) == "L" {
					loc = moveMap[loc].left
				} else {
					loc = moveMap[loc].right
				}
				// }
				stepCountArray[i] += 1
				if isComplete(loc) {
					break
				}
			}
		}
	}

	value := LCM(stepCountArray[0], stepCountArray[1], stepCountArray[2:]...)
	fmt.Printf("The total steps from ending with A to all ending with Z = %d \n", value)
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	part1(lineArray)
	part2(lineArray)
}
