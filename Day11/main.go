package main

import (
	"AdventOfCode/readfile"
	"fmt"
	"math"
)

type location struct {
	x int
	y int
}

type galaxy struct {
	id  int
	loc location
}

func contains(lst []int, val int) bool {
	for _, i := range lst {
		if val == i {
			return true
		}
	}
	return false
}

func findGalaxies(lineArray []string) []galaxy {
	allGalaxies := []galaxy{}
	id := 1
	for y, l := range lineArray {
		if l == "" {
			continue
		}
		for x, r := range l {
			if r == '#' {
				g := galaxy{id, location{x, y}}
				allGalaxies = append(allGalaxies, g)
				id += 1
			}
		}
	}
	return allGalaxies
}

func expandGalaxy(lineArray []string) []string {
	newGalaxy := []string{}
	rows, cols := identifyRowCols(lineArray)

	newY := 0
	for y := range lineArray {
		if lineArray[y] == "" {
			continue
		}
		if contains(rows, y) {
			//add rows if not in rows
			newGalaxy = append(newGalaxy, "...")
			newY += 1
		}
		newGalaxy = append(newGalaxy, "")
		for x, r := range lineArray[y] {
			if contains(cols, x) {
				//add cols if not in cols
				newGalaxy[newY] += "."
			}
			newGalaxy[newY] += string(r)
		}
		newY += 1
	}
	return newGalaxy
}

func identifyRowCols(lineArray []string) ([]int, []int) {
	cols := []int{}
	rows := []int{}
	for y, l := range lineArray {
		for x, r := range l {
			if r == '#' {
				if !contains(rows, y) {
					rows = append(rows, y)
				}
				if !contains(cols, x) {
					cols = append(cols, x)
				}
			}
		}
	}
	//reverse
	revCols := []int{}
	revRows := []int{}
	for i := 0; i < len(lineArray); i++ {
		if !contains(rows, i) {
			revRows = append(revRows, i)
		}
	}
	for i := 0; i < len(lineArray[0]); i++ {
		if !contains(cols, i) {
			revCols = append(revCols, i)
		}
	}
	return revRows, revCols
}

func calcShortestPath(a galaxy, b galaxy) int {
	xDist := math.Abs(float64(b.loc.x) - float64(a.loc.x))
	yDist := math.Abs(float64(b.loc.y) - float64(a.loc.y))
	return int(xDist + yDist)
}

func calcShortestPathExp(a galaxy, b galaxy, multi int, rows []int, cols []int) int {
	xDist := math.Abs(float64(b.loc.x) - float64(a.loc.x))
	yDist := math.Abs(float64(b.loc.y) - float64(a.loc.y))
	// add a multi for each in rows/cols
	for _, r := range rows {
		if r < b.loc.y && r > a.loc.y {
			yDist += float64(multi - 1)
		}
	}
	for _, c := range cols {
		if (c < b.loc.x && c > a.loc.x) || (c < a.loc.x && c > b.loc.x) {
			xDist += float64(multi - 1)
		}
	}
	return int(xDist + yDist)
}

func part1(lineArray []string) {
	newGalaxyArray := expandGalaxy(lineArray)
	allGalaxies := findGalaxies(newGalaxyArray)
	sum := 0
	for i := 0; i < len(allGalaxies); i++ {
		for n := i + 1; n < len(allGalaxies); n++ {
			dist := calcShortestPath(allGalaxies[i], allGalaxies[n])
			sum += dist
			// fmt.Printf("The distance from %d to %d = %d\n", allGalaxies[i].id, allGalaxies[n].id, dist)
		}
	}

	fmt.Printf("The sum of all distances is %d\n", sum)
}

func part2(lineArray []string) {
	//same as above but expand the galaxy by a million for each row
	rows, cols := identifyRowCols(lineArray)
	allGalaxies := findGalaxies(lineArray)
	sum := 0
	for i := 0; i < len(allGalaxies); i++ {
		for n := i + 1; n < len(allGalaxies); n++ {
			dist := calcShortestPathExp(allGalaxies[i], allGalaxies[n], 1000000, rows, cols)
			sum += dist
			// fmt.Printf("The distance from %d to %d = %d\n", allGalaxies[i].id, allGalaxies[n].id, dist)
		}
	}
	fmt.Printf("The sum of all distances is %d\n", sum)
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	part1(lineArray)
	part2(lineArray)
}
