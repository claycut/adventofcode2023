package main

import (
	"AdventOfCode/readfile"
	"fmt"
	"strconv"
	"strings"
)

type race struct {
	time int
	dist int
}

func (r race) countMarginOfError() int {
	count := 0
	for i := 0; i < r.time; i++ {
		thisDist := calcDist(i, r.time)
		if thisDist > r.dist {
			count++
		}
	}
	return count
}

func calcDist(holdTime int, travelTime int) int {
	return (travelTime - holdTime) * holdTime
}

func part1(lineArray []string) {
	timeArray := []int{}
	distArray := []int{}
	for _, l := range lineArray {
		if strings.HasPrefix(l, "Time:") {
			for i, t := range strings.Fields(l) {
				if i == 0 {
					continue
				}
				time, _ := strconv.Atoi(t)
				timeArray = append(timeArray, time)
			}
		} else if strings.HasPrefix(l, "Distance:") {
			for i, d := range strings.Fields(l) {
				if i == 0 {
					continue
				}
				dist, _ := strconv.Atoi(d)
				distArray = append(distArray, dist)
			}
		}
	}
	product := 0
	for i := 0; i < len(timeArray); i++ {
		r := race{timeArray[i], distArray[i]}
		margin := r.countMarginOfError()
		fmt.Printf("Number of ways you can beat the record = %d\n", margin)
		if product == 0 {
			product = margin
		} else {
			product *= margin
		}
	}
	fmt.Printf("The product of those numbers = %d\n", product)
}

func part2(lineArray []string) {
	time := 0
	dist := 0
	for _, l := range lineArray {
		if strings.HasPrefix(l, "Time:") {
			timeStr := strings.ReplaceAll(l, " ", "")
			timeSplit := strings.Split(timeStr, ":")
			time, _ = strconv.Atoi(timeSplit[1])
		} else if strings.HasPrefix(l, "Distance:") {
			distStr := strings.ReplaceAll(l, " ", "")
			distSplit := strings.Split(distStr, ":")
			dist, _ = strconv.Atoi(distSplit[1])
		}
	}

	r := race{time, dist}
	count := r.countMarginOfError()
	fmt.Printf("There are %d number of ways to win the race\n", count)
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	part1(lineArray)
	part2(lineArray)
}
